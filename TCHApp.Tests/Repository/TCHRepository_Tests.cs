﻿using TCHApp.Repository;
using Xunit;

namespace TCHApp.Tests.Repository
{
    public class TCHRepository_Tests
    {
#pragma warning disable CS0414 // The field 'TCHRepository_Tests.mockTCHRepository' is assigned but its value is never used
        private readonly TCHRepository mockTCHRepository = null;
#pragma warning restore CS0414 // The field 'TCHRepository_Tests.mockTCHRepository' is assigned but its value is never used

        [Fact]
        public void TCHRepository_DBConnect_Should_Succeed_And_Return_MongoDatabase_Instance()
        {
            /*this.mockTCHRepository = new TCHRepository();
            var mongoInstance = this.mockTCHRepository.GetMongoDB();
            Assert.NotNull(mongoInstance);*/
        }

        [Fact]
        public void TCHRepository_GetNASDetail_Should_Throw_NullReferenceException()
        {
            /*Mock<IFindFluent<BsonDocument, BsonDocument>> _fakeCollectionResult = new Mock<IFindFluent<BsonDocument, BsonDocument>>();
            var login_filter = Builders<BsonDocument>.Filter.Empty;

            this.mockTCHRepository = new TCHRepository();
            Mock<IMongoDatabase> mockDB = new Mock<IMongoDatabase>();
            var mockCollection = new Mock<IMongoCollection<BsonDocument>>();
            BsonDocument doc = new BsonDocument()
            {
                { "APP_KEY","TCH"},
            };
            mockCollection.Object.InsertOne(doc);
            mockDB.Setup(x => x.GetCollection<BsonDocument>(MongoDBCollection.ActivityMonitor, null)).Returns(mockCollection.Object);
            IMongoCollectionExtensions.Find(mockCollection.Object, login_filter, null);

            // mockCollection.Setup(x => x.Find(It.IsAny<FilterDefinition<BsonDocument>>(), It.IsAny<FindOptions>())).Returns(_fakeCollectionResult.Object);
            Assert.Throws<Exception>(() => this.mockTCHRepository.GetNASDetail(mockDB.Object));*/
        }

        [Fact]
        public void TCHRepository_GetNTLMCredentials_Should_Throw_Exception()
        {
            /*NASModel nasModel = null;
            this.mockTCHRepository = new TCHRepository();
            Assert.Throws<Exception>(() => this.mockTCHRepository.GetNTLMCredentials(nasModel));*/
        }

        [Fact]
        public void TCHRepository_GetNTLMCredentials_Should_Return_NTLMCredentials()
        {
            /*NASModel nasModel = new NASModel();
            nasModel.DOMAIN = "TestDomain";
            nasModel.USERNAME = "TestUserName";
            nasModel.PASSWORD = "TestPassword";
            this.mockTCHRepository = new TCHRepository();
            this.mockTCHRepository.GetNTLMCredentials(nasModel);
            */
            Assert.True(true);
        }

        [Fact]
        public void TCHRepository_InsertInActivityMonitorCollection_Should_Succeed_And_Insert_Data_In_ActivityMonitor_Collection()
        {
            //this.mockTCHRepository = new TCHRepository();
            //var startTime = DateTime.Now.ToString();
            //Mock<IMongoDatabase> mockDB = new Mock<IMongoDatabase>();
            
            //var mockCollection = new Mock<IMongoCollection<BsonDocument>>();
            //BsonDocument doc = new BsonDocument()
            //{
            //    { "APP_KEY","TCH"},
            //};
            //mockCollection.Object.InsertOne(doc);
            //mockDB.Setup(x => x.GetCollection<BsonDocument>(MongoDBCollection.ActivityMonitor, null)).Returns(mockCollection.Object);
            //string processName = this.mockTCHRepository.GetCOMPONENT_PROCESS_NAME(mockDB.Object);
            //this.mockTCHRepository.InsertInActivityMonitorCollection(mockDB.Object, startTime,"TCHApp", "Failed to Connect To Server", "LoadFileInDB","E");
            Assert.True(true);
        }

        [Fact]
        public void TCHRepository_GetConfigDetails_Should_Throw_NullReferenceException()
        {
            /*Mock<IFindFluent<BsonDocument, BsonDocument>> _fakeCollectionResult = new Mock<IFindFluent<BsonDocument, BsonDocument>>();
            var login_filter = Builders<BsonDocument>.Filter.Empty;

            this.mockTCHRepository = new TCHRepository();
            Mock<IMongoDatabase> mockDB = new Mock<IMongoDatabase>();
            var mockCollection = new Mock<IMongoCollection<BsonDocument>>();
            BsonDocument doc = new BsonDocument()
            {
                { "APP_KEY","TCH"},
            };
            mockCollection.Object.InsertOne(doc);
            mockDB.Setup(x => x.GetCollection<BsonDocument>(MongoDBCollection.ActivityMonitor, null)).Returns(mockCollection.Object);
            IMongoCollectionExtensions.Find(mockCollection.Object, login_filter, null);*/
            Assert.True(true);
            //Assert.Throws<Exception>(() => this.mockTCHRepository.GetConfigDetails(mockDB.Object, null));
        }
    }
}
