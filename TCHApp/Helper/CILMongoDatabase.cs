﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TCHApp.Helper
{
    public class CILMongoDatabase
    {

        public List<T> GetMongoCollection<T>(string collectionName)
        {
            List<T> typeList = new List<T>();
            var filter = Builders<BsonDocument>.Filter.Empty;
            try
            {
                List<BsonDocument> bsonDocumentList = RetrieveData(collectionName, typeof(T), filter);

                foreach (var item in bsonDocumentList)
                {
                    typeList.Add(BsonSerializer.Deserialize<T>(item.ToBsonDocument()));
                }

                return typeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> GetMongoCollection<T>(string collectionName, string fieldToFilter, string fieldValue)
        {
            List<BsonDocument> bsonDocumentList = null;
            List<T> typeList = new List<T>();

            try
            {
                if (!string.IsNullOrEmpty(fieldToFilter) && !string.IsNullOrEmpty(fieldValue))
                {
                    var filter = Builders<BsonDocument>.Filter.Eq(fieldToFilter, fieldValue);
                    bsonDocumentList = RetrieveData(collectionName, typeof(T), filter);
                }
                else if (string.IsNullOrEmpty(fieldToFilter) || string.IsNullOrEmpty(fieldValue))
                {
                    var filter = Builders<BsonDocument>.Filter.Empty;
                    bsonDocumentList = RetrieveData(collectionName, typeof(T), filter);
                }

                foreach (var item in bsonDocumentList)
                {
                    typeList.Add(BsonSerializer.Deserialize<T>(item.ToBsonDocument()));
                }

                return typeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> GetMongoCollection<T>(string collectionName, FilterDefinition<T> filter)
        {
            List<T> typeList = new List<T>();

            try
            {
                List<BsonDocument> bsonDocumentList = RetrieveData(collectionName, typeof(T), filter);

                foreach (var item in bsonDocumentList)
                {
                    typeList.Add(BsonSerializer.Deserialize<T>(item.ToBsonDocument()));
                }

                return typeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> GetMongoCollection<T>(string collectionName, FilterDefinition<BsonDocument> filter)
        {
            List<T> typeList = new List<T>();

            try
            {
                List<BsonDocument> bsonDocumentList = RetrieveData(collectionName, typeof(T), filter);

                foreach (var item in bsonDocumentList)
                {
                    typeList.Add(BsonSerializer.Deserialize<T>(item.ToBsonDocument()));
                }

                return typeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<BsonDocument> RetrieveData<T>(string collectionName, Type type, FilterDefinition<T> filter)
        {
            ProjectionDefinitionBuilder<T> projectionBuilder = null;
            ProjectionDefinition<T> projection = null;

            try
            {
                IMongoDatabase mongoDatabase = GetMongoDB();

                // Constructing the object
                Object instance = Activator.CreateInstance(typeof(T));

                string[] fieldsToReturn = instance.GetType().GetProperties().Select(p => p.Name).ToArray();

                if (!fieldsToReturn.Contains("Id"))
                {
                    projectionBuilder = Builders<T>.Projection;
                    projection = projectionBuilder.Combine(fieldsToReturn.Select(field => projectionBuilder.Include(field).Exclude("_id")));
                }
                else
                {
                    projectionBuilder = Builders<T>.Projection;
                    projection = projectionBuilder.Combine(fieldsToReturn.Select(field => projectionBuilder.Include(field)));
                }

                var mongoCollection = mongoDatabase.GetCollection<T>(collectionName);
                var collectionResult = mongoCollection.Find(filter).Project(projection).ToList();
                return collectionResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<BsonDocument> RetrieveData(string collectionName, Type T, FilterDefinition<BsonDocument> filter)
        {
            ProjectionDefinitionBuilder<BsonDocument> projectionBuilder = null;
            ProjectionDefinition<BsonDocument> projection = null;

            try
            {
                IMongoDatabase mongoDatabase = GetMongoDB();
                var mongoCollection = mongoDatabase.GetCollection<BsonDocument>(collectionName);

                if (T == typeof(BsonDocument))
                {
                    var collectionResult1 = mongoCollection.Find(filter).ToList();
                    return collectionResult1;
                }
                else
                {
                    // Constructing the object
                    Object instance = Activator.CreateInstance(T);

                    string[] fieldsToReturn = instance.GetType().GetProperties().Select(p => p.Name).ToArray();

                    if (!fieldsToReturn.Contains("Id"))
                    {
                        projectionBuilder = Builders<BsonDocument>.Projection;
                        projection = projectionBuilder.Combine(fieldsToReturn.Select(field => projectionBuilder.Include(field).Exclude("_id")));
                    }
                    else
                    {
                        projectionBuilder = Builders<BsonDocument>.Projection;
                        projection = projectionBuilder.Combine(fieldsToReturn.Select(field => projectionBuilder.Include(field)));
                    }

                    var collectionResult = mongoCollection.Find(filter).Project(projection).ToList();
                    return collectionResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IMongoDatabase GetMongoDB()
        {
            var strUri = TCHApp.Repository.TCHRepository.strUri;
            var strDb = TCHApp.Repository.TCHRepository.strDb;
            Console.WriteLine("CILMongoDatabase GetMongoDB URI : " + strUri + " Database : " + strDb);
            var _connectionString = strUri;
            var client = new MongoClient(_connectionString);
            var database = client.GetDatabase(strDb);
            return database;
        }
    }
}
