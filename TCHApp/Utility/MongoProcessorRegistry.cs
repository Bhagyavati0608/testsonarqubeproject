﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using StructureMap;
using System.IO;
using Steeltoe.Extensions.Configuration.CloudFoundry; //added for config server
using Steeltoe.Extensions.Configuration.ConfigServer; //added for config server 

namespace TCHApp.Utility
{
   public class MongoProcessorRegistry : Registry
    {
        public MongoProcessorRegistry()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                              .AddCloudFoundry()
                              .AddEnvironmentVariables()
                              .AddConfigServer();

            IConfigurationRoot configuration = builder.Build();
            For<IConfiguration>().Use<IConfigurationRoot>(configuration);
        }

    }


}
