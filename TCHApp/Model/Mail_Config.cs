﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCHApp.Model
{
    public class MailConfig
    {
        public string SMTP { get; set; }
        public string Port { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
    }
}
