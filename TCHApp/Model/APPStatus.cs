﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCHApp.Model
{
    public class APPStatus
    {
        public string APPKEY { get; set; }
        public string SERVICELOCKED { get; set; }
    }
}
