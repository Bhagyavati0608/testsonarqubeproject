﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCHApp.Model
{
    public class NASModel
    {
        public string DOMAIN { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string FILE_PROTOCOL { get; set; }
        public string NAS_LOC { get; set; }
        public string PASSWORDKEY { get; set; }
    }
}
