﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace TCHApp.Model
{
    public class TCHConfig
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string APPKEY { get; set; }
        public string DELIMITER { get; set; }

        public string INPUTGATEWAYPATH { get; set; }
        public string OUTPUTGATEWAYPATH { get; set; }
        public string OUTPUTGATEWAYFILEEXTENSION { get; set; }
     
        public string INPUTFILEEXTENSION { get; set; }
        public string SENDMAIL { get; set; }
        public string EMAILTO { get; set; }
        public string EMAILFROM { get; set; }
        public string EMAILSUBJECT { get; internal set; }
        public string SMTP { get; internal set; }
        public string MAILBODY { get;  set; }
        public string MAXFILESIZE { get; set; }
       
    }
}
