﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCHApp.Model
{
    public class ActivityLog
    {
        public string altbusinessIdentifier { get; set; }
        public string appName { get; set; }
        public string appUser { get; set; }
        public string businessIdentifier { get; set; }
        public string detail { get; set; }
        public string domain { get; set; }
        public string environment { get; set; }
        public string @event { get; set; }
        public string eventCode { get; set; }
        public string eventSubCode { get; set; }
        public string globalTransactionId { get; set; }
        public string hostname { get; set; }
        public string payload { get; set; }
        public string serviceName { get; set; }
        public string serviceVersion { get; set; }
        public string status { get; set; }
        public string step { get; set; }
        public string summary { get; set; }
        public string threadId { get; set; }
        public string transactionMode { get; set; }

    }
}
