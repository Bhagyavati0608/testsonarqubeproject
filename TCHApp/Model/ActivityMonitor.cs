﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCHApp.Model
{
    public class ActivityMonitor
    {
        public string ProcessName { get; set; }

        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string MethodName { get; set; }
        public string ErrorMessage { get; set; }
        public string Status { get; internal set; }
    }
}
