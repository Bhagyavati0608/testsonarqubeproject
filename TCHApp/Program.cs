﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Driver;
using SharpCifs.Smb;
using StructureMap;
using System;
using System.Globalization;
using System.Linq;
using System.Text;
using TCHApp.Constants;
using TCHApp.Interface;
using TCHApp.Model;
using TCHApp.Repository;
using TCHApp.Service;
using TCHApp.Utility;

namespace TCHApp
{
    class Program
    {
        readonly string startTime = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        public string ProcessName = null;
        static IConfiguration configuration;

        static void Main(string[] args)
        {
            using (var container = new Container(c =>
            {
                c.Scan(x =>
                {
                    x.TheCallingAssembly();
                    x.WithDefaultConventions();
                });
                c.AddRegistry(new MongoProcessorRegistry());
            }))
            {
                var _logger = container.GetInstance<TCHRepository>();
                _logger.InitializeConnectionValues();
                 configuration = _logger.GetConfiguration();
            }
            Program program = new Program();
            ITCHRepository tchRepository = program.RegisterServices();
            ActivityLogService activityServiceLog = new ActivityLogService(configuration);

            try
            {
                StringBuilder sb = new StringBuilder();
                program.ProcessName = tchRepository.GetCOMPONENT_PROCESS_NAME(tchRepository.GetMongoDB());
                Console.WriteLine("TCH App Started on : " + DateTime.Now);
                sb.AppendLine("TCH App Started on : " + DateTime.Now);
                // tchRepository.InsertInActivityMonitorCollection(tchRepository.GetMongoDB(), DateTime.Now.ToString(), program.ProcessName, "TCH App Started on: " + DateTime.Now, "Main", "I");
                program.ExecuteTCH(tchRepository, activityServiceLog);
                Console.WriteLine("TCH App Successfully Completed on : " + DateTime.Now);
                sb.AppendLine("TCH App Successfully Completed on : " + DateTime.Now);
                tchRepository.InsertInActivityMonitorCollection(tchRepository.GetMongoDB(), DateTime.Now.ToString(CultureInfo.InvariantCulture), program.ProcessName, sb.ToString(), "Main", "I");
                System.Threading.Thread.Sleep(2000 * 60);

            }

            catch (Exception ex)
            {
                Console.WriteLine("Test");
                Console.WriteLine("TCH App Failed due to " + ex.Message);
                tchRepository.InsertInActivityMonitorCollection(tchRepository.GetMongoDB(), DateTime.Now.ToString(CultureInfo.InvariantCulture), program.ProcessName, "TCH App Failed due to " + ex.Message, "Main", "E");
            }
        }

        public ITCHRepository RegisterServices()
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddSingleton<ITCHRepository, TCHRepository>()
                .BuildServiceProvider();

            //do the actual work here
            var tchRepository = serviceProvider.GetService<ITCHRepository>();
            return tchRepository;
        }

        public void ExecuteTCH(ITCHRepository tchRepository, ActivityLogService activityServiceLog)
        {
            IMongoDatabase mongoDB = null;
            TCHConfig tchConfig = null;
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("Execute TCH Method started");
                mongoDB = tchRepository.GetMongoDB();
                NASModel nasModel = tchRepository.GetNASDetail(mongoDB);
                tchConfig = tchRepository.GetConfigDetails(mongoDB, nasModel.PASSWORDKEY);
                APPStatus appStatus = tchRepository.GetStatusDetails(mongoDB);

                ServiceURL serviceURLs = tchRepository.GetServiceURL(mongoDB);
                Console.WriteLine("Environment: " + tchConfig.INPUTGATEWAYPATH); //added for config server testing
                if (appStatus.SERVICELOCKED == "N")
                {
                    //adding to set the service_locked status, start
                    IMongoCollection<BsonDocument> app_status_collection = mongoDB.GetCollection<BsonDocument>(MongoDBCollection.AppStatus);

                    // string service_locked = string.Empty;
                    string app_param = string.Empty;

                    //set service_locked as Y
                    app_param = "{$set: { SERVICE_LOCKED:'Y' } }";
                    tchRepository.UpdateAppStatusCollection(app_status_collection, app_param);
                    Console.WriteLine("Service Locked Status : " + appStatus.SERVICELOCKED);
                    sb.AppendLine("Service Locked Status : " + appStatus.SERVICELOCKED);
                    Console.WriteLine("Calling TCH Service.");
                    sb.AppendLine("Calling TCH Service.");
                    string generatedFileStatus = LoadInputFileAndSendMail(tchRepository, mongoDB, activityServiceLog, tchConfig, appStatus, serviceURLs, nasModel);
                    app_param = "{$set: { SERVICE_LOCKED:'N' } }";
                    tchRepository.UpdateAppStatusCollection(app_status_collection, app_param);
                    sb.AppendLine("Execute TCH Method Successfully completed");
                    //  tchRepository.InsertInActivityMonitorCollection(mongoDB, startTime, ProcessName, "Execute TCH Method Successfully completed", "ExecuteTCH", "I");
                }
                else
                {
                    Console.WriteLine("Service is Locked");
                    sb.AppendLine("Service is Locked");
                    // tchRepository.InsertInActivityMonitorCollection(mongoDB, startTime, ProcessName, "Service is Locked", "ExecuteTCH", "I");
                }
                tchRepository.InsertInActivityMonitorCollection(mongoDB, startTime, ProcessName, sb.ToString(), "ExecuteTCH", "I");
            }
            catch (Exception ex)
            {
                tchRepository.InsertInActivityMonitorCollection(mongoDB, startTime, ProcessName, sb.ToString(), "ExecuteTCH", "I");
                Console.WriteLine(ex.Message);
                tchRepository.InsertInActivityMonitorCollection(mongoDB, startTime, ProcessName, ex.Message, "ExecuteTCH", "E");
                tchRepository.SendNotificationMail(mongoDB, ex, "ExecuteTCH");
            }
        }

        private string LoadInputFileAndSendMail(ITCHRepository tchRepository, IMongoDatabase mongoDatabase, ActivityLogService activityServiceLog, TCHConfig tchConfig, APPStatus appStatus, ServiceURL serviceURLs, NASModel nasModel)
        {
            string insertedStatus = "False";

            try
            {
                try
                {
                    StringBuilder sb = new StringBuilder();
                    var credentials = tchRepository.GetNTLMCredentials(nasModel);
                    var inputFileLocation = new SmbFile(@"smb:" + tchConfig.INPUTGATEWAYPATH, credentials);
                    var searchFiles = "*" + tchConfig.INPUTFILEEXTENSION;
                    int filesCount = 0;
                    try
                    {
                        filesCount = inputFileLocation.ListFiles(searchFiles).Length;
                    }
                    catch (Exception )
                    {
                        throw;
                    }
                    if (filesCount > 0)
                    {
                        Console.WriteLine("File count :" + filesCount);
                        sb.AppendLine("File count :" + filesCount);
                        foreach (SmbFile file in inputFileLocation.ListFiles(searchFiles).OrderBy(s => s.CreateTime()))
                        {
                            //string url = "https://localhost:44324/api/TCH/LoadInputFile?inputFilePath=" + file;
                            string url = serviceURLs.TCH + "api/TCH/LoadInputFile?inputFilePath=" + file;
                            Console.WriteLine("api/TCH/LoadInputFile?inputFilePath=" + file);
                            sb.AppendLine("api/TCH/LoadInputFile?inputFilePath=" + file);
                            insertedStatus = tchRepository.GetHttpWebResponse(startTime, activityServiceLog, mongoDatabase, url);
                            Console.WriteLine("Output File Status: " + insertedStatus);
                            sb.AppendLine("Output File Status: " + insertedStatus);
                            // tchRepository.InsertInActivityMonitorCollection(tchRepository.GetMongoDB(), DateTime.Now.ToString(), ProcessName, "Output File Status: " + insertedStatus.ToString(), "LoadInputFileAndSendMail", "I");
                        }
                        sb.AppendLine("LoadInputFileAndSendMail Successfully Completed");
                        //  tchRepository.InsertInActivityMonitorCollection(mongoDatabase, startTime, ProcessName, "LoadInputFileAndSendMail Successfully Completed", "LoadInputFileAndSendMail", "I");
                    }
                    else
                    {
                        Console.WriteLine("TCH Input File does not exists in " + RelativePath(tchConfig.INPUTGATEWAYPATH));
                        sb.AppendLine("TCH Input File does not exists in " + RelativePath(tchConfig.INPUTGATEWAYPATH));
                        //  tchRepository.InsertInActivityMonitorCollection(mongoDatabase, startTime, ProcessName, "TCH Input File does not exists in " + RelativePath(tchConfig.INPUT_GATEWAY_PATH), "LoadInputFileAndSendMail", "I");
                    }
                    tchRepository.InsertInActivityMonitorCollection(mongoDatabase, startTime, ProcessName, sb.ToString(), "LoadInputFileAndSendMail", "I");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    tchRepository.InsertInActivityMonitorCollection(mongoDatabase, startTime, ProcessName, ex.Message, "LoadInputFileAndSendMail", "E");
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                insertedStatus = "False";
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error in TCH App LoadInputFileAndSendMail Method");
                sb.AppendLine("Error Message : " + ex.Message);
                tchRepository.InsertInActivityMonitorCollection(mongoDatabase, startTime, ProcessName, sb.ToString(), "LoadInputFileAndSendMail", "E");
                throw new Exception(sb.ToString());
            }
            return insertedStatus;
        }

        private string RelativePath(string path)
        {
            return path.Split(new string[] { "Concur" }, StringSplitOptions.None).TakeLast(1).ToList().FirstOrDefault();
        }
    }
}
