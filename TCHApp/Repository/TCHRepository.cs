﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using SharpCifs.Smb;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using TCHApp.Constants;
using TCHApp.Helper;
using TCHApp.Interface;
using TCHApp.Model;
using TCHApp.Service;

namespace TCHApp.Repository
{
    public class TCHRepository : ITCHRepository
    {
        CILMongoDatabase commonHelper = null;
        internal static string strUri = "";
        internal static string strDb = "";
        private readonly IConfiguration _configuration;

        public TCHRepository()
        {
            commonHelper = new CILMongoDatabase();           
        }

        public TCHRepository(IConfiguration configuration)
        {           
            _configuration = configuration;
        }

        public IConfiguration GetConfiguration()
        {
            return _configuration;
        }
        public void InitializeConnectionValues()
        {
            strUri = _configuration.GetConnectionString("Server"); 
            strDb = _configuration.GetConnectionString("Database");            
        }

        public virtual IMongoDatabase GetMongoDB()
        {
            try
            {
                Console.WriteLine("TCHRepository GetMongoDB URI : " + strUri + " Database : " + strDb);
                var _connectionString = strUri;
                var client = new MongoClient(_connectionString);
                IMongoDatabase _database = client.GetDatabase(strDb);
                return _database;

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error Occured In TCH App Repository at GetMongoDB Method.");
                sb.AppendLine("Error Message : " + ex.Message);

                Console.WriteLine(sb.ToString());
                throw new Exception(sb.ToString());
            }
        }

        public virtual NASModel GetNASDetail(IMongoDatabase mongoDatabase)
        {
            IMongoCollection<BsonDocument> login_info_collection;
            NASModel nasModel = null;
            try
            {
                Dell.CIL.EncryptDecrypt decryptObj = new Dell.CIL.EncryptDecrypt();
                if (mongoDatabase != null)
                {

                    login_info_collection = mongoDatabase.GetCollection<BsonDocument>(MongoDBCollection.LoginInfo);
                    var login_filter = Builders<BsonDocument>.Filter.Empty;
                    var login_result = login_info_collection.Find(login_filter).ToList();

                    if (login_result.Count == 1)
                    {
                        foreach (var login_detail in login_result)
                        {
                            nasModel = new NASModel()
                            {
                                DOMAIN = Convert.ToString(login_detail.GetValue("DOMAIN"), CultureInfo.CurrentCulture),
                                USERNAME = Convert.ToString(login_detail.GetValue("USERNAME"), CultureInfo.CurrentCulture),
                                PASSWORD = decryptObj.Decrypt(Convert.ToString(login_detail.GetValue("PASSWORD_KEY"), CultureInfo.CurrentCulture), Convert.ToString(login_detail.GetValue("PASSWORD"), CultureInfo.CurrentCulture)),
                                FILE_PROTOCOL = Convert.ToString(login_detail.GetValue("FILE_PROTOCOL"), CultureInfo.CurrentCulture),
                                NAS_LOC = Convert.ToString(login_detail.GetValue("NAS_LOC"), CultureInfo.CurrentCulture),
                                PASSWORDKEY = Convert.ToString(login_detail.GetValue("PASSWORD_KEY"), CultureInfo.CurrentCulture),
                            };
                        }
                    }
                    else if (login_result.Count > 1)
                    {
                        throw new Exception("More than one NAS Credentials are found in LOGIN_INFO Collection in MongoDB");
                    }
                    else if (login_result.Count == 0)
                    {
                        throw new Exception("NAS Credentials not found in LOGIN_INFO Collection in MongoDB");
                    }
                }
                
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error Occured While Connecting to NAS Server.");
                sb.AppendLine("Error Message : " + ex.Message);

                Console.WriteLine(sb.ToString());
                throw new Exception(sb.ToString());
            }

            return nasModel;
        }

        public virtual NtlmPasswordAuthentication GetNTLMCredentials(NASModel nasModel)
        {
            try
            {
                if(nasModel!=null)
                {
                    return new NtlmPasswordAuthentication(nasModel.DOMAIN, nasModel.USERNAME, nasModel.PASSWORD);
                }
                else
                {
                    return new NtlmPasswordAuthentication("");

                }
                   
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error Occured In TCH App Repository at GetNTLMCredentials Method.");
                sb.AppendLine("Error Message : " + ex.Message);

                Console.WriteLine(sb.ToString());
                throw new Exception(sb.ToString());
            }
        }

        public virtual TCHConfig GetConfigDetails(IMongoDatabase mongoDatabase, string passwordKey)
        {
            TCHConfig apConfig = null;
            try
            {
                var filter = Builders<BsonDocument>.Filter.Eq("APP_KEY", "TCH");
                TCHConfig tchConfig = commonHelper.GetMongoCollection<TCHConfig>(MongoDBCollection.Config, filter).FirstOrDefault();
                return tchConfig;
            }
            catch (Exception ex)
            {
                apConfig = new TCHConfig();

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error Occured While Retrieving TCH CONFIG Collection.");
                sb.AppendLine("Error Message : " + ex.Message);

                Console.WriteLine(sb.ToString());
                throw new Exception(sb.ToString());
            }
        }

        public virtual APPStatus GetStatusDetails(IMongoDatabase mongoDatabase)
        {
            APPStatus appStatus = null;
            try
            {
                var filterStatus = Builders<BsonDocument>.Filter.Eq("APP_KEY", "TCH");
                appStatus = commonHelper.GetMongoCollection<APPStatus>(MongoDBCollection.AppStatus, filterStatus).FirstOrDefault();
                return appStatus;

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error Occured While Retrieving TCH APP_STATUS Collection.");
                sb.AppendLine("Error Message : " + ex.Message);

                Console.WriteLine(sb.ToString());
                throw new Exception(sb.ToString());
            }
        }

        public virtual void InsertInActivityMonitorCollection(IMongoDatabase mongoDatabase, string startTime, string sourceName, string errorMessage, string targetSiteName,string status)
        {
            if(mongoDatabase !=null)
            {
                var exceptionCollection = mongoDatabase.GetCollection<BsonDocument>(MongoDBCollection.ActivityMonitor);
                var activityModel = new ActivityMonitor()
                {
                    ProcessName = sourceName,
                    StartTime = startTime,
                    EndTime = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    MethodName = targetSiteName,
                    ErrorMessage = errorMessage,
                    Status = status
                };

                exceptionCollection.InsertOne(activityModel.ToBsonDocument());
            }
        
        }

        public static MailConfig GetMailDetails(IMongoDatabase mongoDatabase)
        {
            IMongoCollection<BsonDocument> collection;
            MailConfig mailConfig = null;
            try
            {
                if (mongoDatabase != null)
                {
                    collection = mongoDatabase.GetCollection<BsonDocument>(MongoDBCollection.MailConfig);
                    var filter = Builders<BsonDocument>.Filter.Empty;
                    var resultStatus = collection.Find(filter).ToList();
                    foreach (var field in resultStatus)
                    {
                        mailConfig = new MailConfig()
                        {

                            SMTP = Convert.ToString(field.GetValue("SMTP"), CultureInfo.CurrentCulture),
                            Port = Convert.ToString(field.GetValue("PORT"), CultureInfo.CurrentCulture),
                            From = Convert.ToString(field.GetValue("FROM"), CultureInfo.CurrentCulture),
                            To = Convert.ToString(field.GetValue("TO"), CultureInfo.CurrentCulture),
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Occured in GetMailDetails Method. \n Error Message : " + ex.Message);
                throw new Exception("Error Occured in GetMailDetails Method. \n Error Message : " + ex.Message);
            }
            return mailConfig;
        }

        public void UpdateAppStatusCollection(IMongoCollection<BsonDocument> app_status_collection, string app_param)
        {
            var app_status_filter = "{'APP_KEY': 'TCH'}";

            try
            {
                if(app_status_collection!=null)
                {
                    //****** convert filter and updating value to BsonDocument*******
                    BsonDocument filterDoc = BsonDocument.Parse(app_status_filter);
                    BsonDocument document = BsonDocument.Parse(app_param);

                    //********Update value using UpdateOne method*****
                    app_status_collection.UpdateOne(filterDoc, document);
                }
               
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Unable to update Status in APP_STATUS Collection.");
                sb.AppendLine("Error Message : " + ex.Message);

                Console.WriteLine(sb.ToString());
                throw new Exception(sb.ToString());
            }
        }

        public void SendNotificationMail(IMongoDatabase mongoDatabase, Exception ex, string methodName)
        {
            try
            {
                string Processname = GetCOMPONENT_PROCESS_NAME(mongoDatabase);
                MailConfig mailConfig = GetMailDetails(mongoDatabase);
                if (ex != null)
                {
                    Dell.CIL.Email email = new Dell.CIL.Email(mailConfig.SMTP, Convert.ToInt32(mailConfig.Port,CultureInfo.InvariantCulture))
                    {
                        From = mailConfig.From,
                        To = mailConfig.To,
                        Subject = Processname + " - " + "Failed",
                        Body = ConstructBody(ex, methodName)
                    };
                    email.Send();
                }
                else
                {
                    Console.WriteLine("Argument Exception ex has null value");
                }
            }
            catch (Exception exp)
            {

                Console.WriteLine(exp.Message);
            }
        }

        public string GetCOMPONENT_PROCESS_NAME(IMongoDatabase mongoDatabase)
        {
            IMongoCollection<BsonDocument> collection;
            string ProcessName = null;
            try
            {
                if(mongoDatabase!=null)
                {
                    collection = mongoDatabase.GetCollection<BsonDocument>(MongoDBCollection.componetProcessName);
                    var filterStatus = Builders<BsonDocument>.Filter.Eq("APP_KEY", "TCH_APP");
                    var resultStatus = collection.Find(filterStatus).ToList();
                    foreach (var field in resultStatus)
                    {
                        ProcessName = Convert.ToString(field.GetValue("PROCESS_NAME"),CultureInfo.InvariantCulture);
                    }
                }
                
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error Occured While Retrieving TCH GetCOMPONENT_PROCESS_NAME Collection.");
                sb.AppendLine("Error Message : " + ex.Message);

                Console.WriteLine(sb.ToString());
                throw new Exception(sb.ToString());
            }
            return ProcessName;
        }

        #region Non DB Methods
        private static string ConstructBody(Exception ex, string methodName)
        {
            StringBuilder sb = new StringBuilder();
            //sb.AppendLine("Source" + ex.Source);
            //sb.AppendLine("Method Name : " + methodName);
            //sb.AppendLine("Exception Type : " + ex.GetType().ToString());
            //sb.AppendLine("Target Site" + ex.TargetSite.Name);
            //sb.AppendLine("Error Details : " + ex.Message);
            sb.AppendLine("<HTML>");
            sb.AppendLine("<body>");
            sb.AppendLine("<style>");
            sb.AppendLine("td{font-family:Verdana;font-size:11px} ");
            sb.AppendLine(".ReportHeader{font-family:verdana;font-size:18px;font-weight:bold;border:1px solid #c9c9c9;text-align:center;height:20px;background-color:#CCCCCC}");
            sb.AppendLine(".ReportItem{font-family:verdana;font-size:18px;padding-left:3px;border:1px solid #cccccc;padding:5px;text-align:left;height:20px;}");
            sb.AppendLine("</style> ");
            sb.AppendLine("<table cellspacing=0 cellpadding=0 style='width:60%'> ");
            sb.AppendLine("<tr><td style='width:15%'></td><td style='width:45%'></td></tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td colspan='2' style='font-size:18px'><i><b><u>Details below: (" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss",CultureInfo.InvariantCulture) + ")</u></b></i><br /><br /></td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='ReportHeader' align='center'>&nbsp;&nbsp;Exception&nbsp;Details&nbsp;&nbsp;</td>");
            sb.AppendLine("<td class='ReportHeader' align='center'>Error&nbsp;Message</td>");
            sb.AppendLine("</tr> ");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='ReportItem' align='center'>Source</td>");
            sb.AppendLine("<td class='ReportItem' align='center'>" + ex.Source + "</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='ReportItem' align='center'>Method&nbsp;Name</td>");
            sb.AppendLine("<td class='ReportItem' align='center'>" + methodName + "</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='ReportItem' align='center'>Exception&nbsp;Type</td>");
            sb.AppendLine("<td class='ReportItem' align='center'>" + ex.GetType().ToString() + "</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='ReportItem' align='center'>Target&nbsp;Site</td>");
            sb.AppendLine("<td class='ReportItem' align='center'>" + ex.TargetSite.Name + "</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='ReportItem' align='center'>Error&nbsp;Message</td>");
            sb.AppendLine("<td class='ReportItem' align='center'>" + ex.Message + "</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("</table>");
            sb.AppendLine("</body>");
            sb.AppendLine("</HTML>");
            return sb.ToString();
        }

        public string GetHttpWebResponse(string startTime, ActivityLogService activityServiceLog, IMongoDatabase mongoDB, string url)
        {
            string responseString = "False";
            string Processname = GetCOMPONENT_PROCESS_NAME(mongoDB);
            ServicePointManager.ServerCertificateValidationCallback =
                              (object a, System.Security.Cryptography.X509Certificates.X509Certificate b, System.Security.Cryptography.X509Certificates.X509Chain c, System.Net.Security.SslPolicyErrors d) => { return true; };

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Timeout = Timeout.Infinite;
                req.KeepAlive = true;
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                responseString = GetServiceResponse(resp);
            }
            catch (WebException e)
            {
                string text = GetServiceErrorMessage(e);
                throw new Exception(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Occured in TCH App Repository at GetHttpWebResponse Method. \n Error Message : " + ex.Message);
                InsertInActivityMonitorCollection(mongoDB, startTime, Processname, ex.Message, "GetHttpWebResponse","E");
                throw new Exception("Error Occured in TCH App Repository at GetHttpWebResponse Method. \n Error Message : " + ex.Message);
            }

            return responseString;
        }

        private string GetServiceErrorMessage(WebException e)
        {
            string text;
            using (WebResponse response = e.Response)
            {
                HttpWebResponse httpResponse = (HttpWebResponse)response;
                Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                using (Stream data = response.GetResponseStream())
                {
                    text = new StreamReader(data).ReadToEnd();
                    Console.WriteLine(text);
                }
            }

            return text;
        }

        private string GetServiceResponse(HttpWebResponse resp)
        {
            string responseText = "False";
            switch (resp.StatusCode)
            {
                case HttpStatusCode.OK:
                    responseText = "True";
                    break;
                default:
                    using (Stream data = resp.GetResponseStream())
                    {
                        responseText = new StreamReader(data).ReadToEnd();
                        Console.WriteLine(responseText);
                    }
                    break;
            }
            return responseText;
        }

        public ServiceURL GetServiceURL(IMongoDatabase mongoDB)
        {
            ServiceURL serviceUrl = null;

            try
            {
                if(mongoDB !=null)
                {
                    var serviceURLCollection = mongoDB.GetCollection<BsonDocument>(MongoDBCollection.ServiceURL);
                    var filter = Builders<BsonDocument>.Filter.Empty;
                    var serviceURL_Result = serviceURLCollection.Find(filter).ToList();
                    serviceUrl = new ServiceURL();
                    foreach (var field in serviceURL_Result)
                    {
                        if (field.GetValue("APP_KEY") == "TCH")
                        {
                            serviceUrl.TCH = Convert.ToString(field.GetValue("APP_URL"),CultureInfo.InvariantCulture);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Occured in TCH App Repository at GetServiceURL Method.");
                Console.WriteLine("SERVICE_URL not found for TCH Service in Mongo Database.");
                throw new Exception("Error Occured in TCH App Repository at GetServiceURL Method.\n SERVICE_URL not found for TCHService in Mongo Database.\n Error Message : " + ex.Message);
            }
            return serviceUrl;
        }

        #endregion
    }

}

