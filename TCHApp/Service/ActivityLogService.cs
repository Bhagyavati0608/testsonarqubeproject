﻿using TCHApp.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
namespace TCHApp.Service
{
    public class ActivityLogService
    {
        private readonly string _loggingUrl;
        private readonly string _apiKey;
        private readonly ActivityLog _loggingData = new ActivityLog();

        public ActivityLogService(IConfiguration config)
        {
            if(config!=null)
            {
                _loggingUrl = config.GetSection("Logging")["LoggingApiUrl"];
                _apiKey = config.GetSection("Logging")["LoggingApiKey"];

                _loggingData.appName = config.GetSection("Logging")["AppName"];
                _loggingData.appUser = config.GetSection("Logging")["AppUser"];
                _loggingData.domain = config.GetSection("Logging")["Domain"];
                _loggingData.environment = config.GetSection("Logging")["Environment"];
                _loggingData.hostname = config.GetSection("Logging")["Hostname"];
                _loggingData.serviceName = config.GetSection("Logging")["ServiceName"];
                _loggingData.serviceVersion = config.GetSection("Logging")["ServiceVersion"];
            }
            
        }

        public bool LogError(string businessIdentifier, string globalTransactionId
            , string status, string step, string threadId, string transactionMode
            , string @event = "SERVICE", string payload = "", string summary = ""
            , string altbusinessIdentifier = "", string detail = "", string eventCode = ""
            , string eventSubCode = "")
        {
            try
            {
                _loggingData.altbusinessIdentifier = altbusinessIdentifier;
                _loggingData.businessIdentifier = businessIdentifier;
                _loggingData.detail = detail;
                _loggingData.@event = @event;
                _loggingData.eventCode = eventCode;
                _loggingData.eventSubCode = eventSubCode;
                _loggingData.globalTransactionId = globalTransactionId;
                _loggingData.payload = payload;
                _loggingData.status = status;
                _loggingData.step = step;
                _loggingData.summary = summary;
                _loggingData.threadId = threadId;
                _loggingData.transactionMode = transactionMode;

                using (HttpClient client = new HttpClient())
                {
                    using (StringContent content = new StringContent(JsonConvert.SerializeObject(_loggingData)))
                    {
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("apikey", _apiKey);
                        var response = client.PostAsync(_loggingUrl, content);
                    }                   
                }                   
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

    }
}
