﻿using TCHApp.Model;
using TCHApp.Service;

using MongoDB.Bson;
using MongoDB.Driver;
using SharpCifs.Smb;
using System;
using System.Collections.Generic;
using System.Text;

namespace TCHApp.Interface
{
    public interface ITCHRepository
    {
        IMongoDatabase GetMongoDB();
        NASModel GetNASDetail(IMongoDatabase mongoDB);
        NtlmPasswordAuthentication GetNTLMCredentials(NASModel dbModel);
        void InsertInActivityMonitorCollection(IMongoDatabase mongoDB, string startTime, string sourceName, string errorMessage, string targetSiteName, string status);
        TCHConfig GetConfigDetails(IMongoDatabase mongoDatabase, string passwordKey);
        void SendNotificationMail(IMongoDatabase mongoDatabase, Exception ex, string methodName);

        string GetHttpWebResponse(string startTime, ActivityLogService activityServiceLog, IMongoDatabase mongoDB, string url);
        ServiceURL GetServiceURL(IMongoDatabase mongoDB);
        APPStatus GetStatusDetails(IMongoDatabase mongoDatabase);
        void UpdateAppStatusCollection(IMongoCollection<BsonDocument> appStatusCollection, string appParam);
        string GetCOMPONENT_PROCESS_NAME(IMongoDatabase mongoDatabase);
        // void InitializeConnectionValues();
    }
}
