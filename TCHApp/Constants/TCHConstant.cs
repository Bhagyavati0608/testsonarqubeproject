﻿namespace TCHApp.Constants
{
   

        public static class MongoDBCollection
        {
            public const string Config = "CONFIG";
            public const string LoginInfo = "LOGIN_INFO";
            public const string AppStatus = "APP_STATUS";
            public const string AppException = "APP_EXCEPTION";
            public const string ActivityMonitor = "ACTIVITY_MONITOR";
            public const string MailConfig = "MAIL_CONFIG";
            public const string ServiceURL = "SERVICE_URL";
            public const string componetProcessName = "COMPONENT_PROCESS_NAME";
        }
    
}
